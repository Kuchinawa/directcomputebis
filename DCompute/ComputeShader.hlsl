struct BufType
{
    int i;
    float f;
};

StructuredBuffer<BufType> Buffer0 : register(t0);
RWStructuredBuffer<BufType> BufferOut : register(u0);

//Threads in group
[numthreads(1, 1, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
    BufferOut[DTid.x].i = (Buffer0[DTid.x].i *2);
    BufferOut[DTid.x].f = (Buffer0[DTid.x].f *2);


}