// DCompute.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using Microsoft::WRL::ComPtr;
using std::unique_ptr;
using std::vector;
using std::string;


bool InitializeDevice(ComPtr<ID3D11Device>& device, ComPtr<ID3D11DeviceContext>& deviceContext)
{
    const D3D_FEATURE_LEVEL lvl[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0
    };

    UINT createDeviceFlags = 0;

#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    HRESULT hr =
        D3D11CreateDevice(
            nullptr,
            D3D_DRIVER_TYPE_HARDWARE,
            nullptr,
            createDeviceFlags,
            lvl,
            _countof(lvl),
            D3D11_SDK_VERSION,
            &device,
            nullptr,
            &deviceContext);

    if (hr == E_INVALIDARG)
    {
        // DirectX 11.0 Runtime doesn't recognize D3D_FEATURE_LEVEL_11_1 as a valid value
        hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, 0, &lvl[1], _countof(lvl) - 1, D3D11_SDK_VERSION, &device, nullptr, &deviceContext);
    }

    if (FAILED(hr))
    {
        printf("Failed creating Direct3D 11 device %08X\n", hr);
        return false;
    }



}

bool InitializeShader(const ComPtr<ID3D11Device> device, const string csFilename, ComPtr<ID3D11ComputeShader>& computeShader)
{
    HRESULT hResult;

    //Verify compute shader is supported
    if (device->GetFeatureLevel() < D3D_FEATURE_LEVEL_11_0)
    {
        D3D11_FEATURE_DATA_D3D10_X_HARDWARE_OPTIONS hwopts = { 0 };
        (void)device->CheckFeatureSupport(D3D11_FEATURE_D3D10_X_HARDWARE_OPTIONS, &hwopts, sizeof(hwopts));

        if (!hwopts.ComputeShaders_Plus_RawAndStructuredBuffers_Via_Shader_4_x)
        {
            printf("DirectCompute is not supported by this device\n");
            return false;
        }
    }

    //Load the compute shader bytecode and create the compute shader from it
    std::ifstream csStream;
    size_t csSize;
    unique_ptr<char[]> csData = nullptr;

    csStream.open(csFilename, std::ifstream::in | std::ifstream::binary);

    if (csStream.good())
    {
        csStream.seekg(0, std::ios::end);
        csSize = size_t(csStream.tellg());
        csData = unique_ptr<char[]>(new char[csSize]);
        csStream.seekg(0, std::ios::beg);
        csStream.read(csData.get(), csSize);
        csStream.close();

        hResult = device->CreateComputeShader(csData.get(), csSize, nullptr, &computeShader);

        csData = nullptr;
        if (FAILED(hResult))
        {
            return false;
        }

#if defined(_DEBUG) || defined(PROFILE)
        if (SUCCEEDED(hResult))
        {
            computeShader->SetPrivateData(WKPDID_D3DDebugObjectName, lstrlenA("main"), "main");
        }
#endif
    }
    else
    {
        return false;
    }

    printf("Compute Shader successfully created.\n");
}

HRESULT CreateStructuredBuffer(ComPtr<ID3D11Device> device, UINT elementSize, UINT count, void* initialData, ComPtr<ID3D11Buffer>& bufferOut)
{
    // Create a buffer to be bound as Compute Shader input (D3D11_BIND_SHADER_RESOURCE).
    D3D11_BUFFER_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    //desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
    //    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.ByteWidth = elementSize * count;
    desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
    desc.StructureByteStride = elementSize;

    if (initialData)
    {
        D3D11_SUBRESOURCE_DATA initData;
        initData.pSysMem = initialData;
        return device->CreateBuffer(&desc, &initData, bufferOut.GetAddressOf());
    }
    else
    {
        return device->CreateBuffer(&desc, nullptr, bufferOut.GetAddressOf());
    }
}

//Create Structured Buffer View
HRESULT CreateBufferSRV(ComPtr<ID3D11Device> device, ComPtr<ID3D11Buffer> buffer, ComPtr<ID3D11ShaderResourceView>& SRVOut)
{
    D3D11_BUFFER_DESC buffDesc;
    ZeroMemory(&buffDesc, sizeof(buffDesc));
    buffer->GetDesc(&buffDesc);

    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    ZeroMemory(&srvDesc, sizeof(srvDesc));
    srvDesc.Format = DXGI_FORMAT_UNKNOWN;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
    srvDesc.BufferEx.FirstElement = 0;
    srvDesc.BufferEx.NumElements = buffDesc.ByteWidth / buffDesc.StructureByteStride;

    return device->CreateShaderResourceView(buffer.Get(), &srvDesc, SRVOut.GetAddressOf());
}

//Create Unordered Access View
HRESULT CreateBufferUAV(ComPtr<ID3D11Device> device, ComPtr<ID3D11Buffer> buffer, ComPtr<ID3D11UnorderedAccessView>& UAVOut)
{
    D3D11_BUFFER_DESC buffDesc;
    ZeroMemory(&buffDesc, sizeof(buffDesc));
    buffer->GetDesc(&buffDesc);

    D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
    ZeroMemory(&uavDesc, sizeof(uavDesc));
    uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
    uavDesc.Buffer.FirstElement = 0;
    uavDesc.Buffer.NumElements = buffDesc.ByteWidth / buffDesc.StructureByteStride;
    uavDesc.Format = DXGI_FORMAT_UNKNOWN; //Must be DXGI_FORMAT_UNKNOWN for structured buffers

    return device->CreateUnorderedAccessView(buffer.Get(), &uavDesc, &UAVOut);
}

ComPtr<ID3D11Buffer> CreateAndRetrieveResultBuffer(ComPtr<ID3D11Device> device, ComPtr<ID3D11DeviceContext> deviceContext, ComPtr<ID3D11Buffer> buffer)
{
    ComPtr<ID3D11Buffer> outBuffer = nullptr;

    D3D11_BUFFER_DESC buffDesc;
    ZeroMemory(&buffDesc, sizeof(buffDesc));
    buffer->GetDesc(&buffDesc);
    buffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    buffDesc.Usage = D3D11_USAGE_STAGING;
    buffDesc.BindFlags = 0;
    buffDesc.MiscFlags = 0;

    if (SUCCEEDED(device->CreateBuffer(&buffDesc, nullptr, outBuffer.GetAddressOf())))
    {
#if defined(_DEBUG)
        outBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof("Debug") - 1, "Debug");
#endif

        deviceContext->CopyResource(outBuffer.Get(), buffer.Get());
    }

    return outBuffer;
}

int main()
{
#ifdef _DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif    

    HRESULT hResult;

    ComPtr<ID3D11Device> device = nullptr;
    ComPtr<ID3D11DeviceContext> deviceContext = nullptr;

    InitializeDevice(device, deviceContext);

    string csFilename = "ComputeShader.cso";
    ComPtr<ID3D11ComputeShader> computeShader = nullptr;

    InitializeShader(device, csFilename, computeShader);

    struct BufType
    {
        int i;
        float f;
    };

    vector<BufType> countVec;

    for (int i = 1; i < 11; ++i)
    {
        countVec.push_back({ i, float(i) });
    }

    //Create a structured input buffer and fill it with our initial values
    ComPtr<ID3D11Buffer> inputBuffer;
    hResult = CreateStructuredBuffer(device, sizeof(BufType), countVec.size(), countVec.data(), inputBuffer);

    if (FAILED(hResult))
    {
        return -1;
    }

    printf("Output buffer successfully created.\n");



    ComPtr<ID3D11Buffer> outputBuffer;
    hResult = CreateStructuredBuffer(device, sizeof(BufType), countVec.size(), nullptr, outputBuffer);

    if (FAILED(hResult))
    {
        return -1;
    }
    printf("Output buffer successfully created.\n");

#if defined(_DEBUG)
    if (inputBuffer)
        inputBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof("Buffer0") - 1, "Buffer0");
    if (outputBuffer)
        outputBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof("BufferOut") - 1, "BufferOut");
#endif

    ComPtr<ID3D11ShaderResourceView> inputView;
    hResult = CreateBufferSRV(device, inputBuffer, inputView);

    if (FAILED(hResult))
    {
        return -1;
    }
    printf("Shader Resource Views successfully created.\n");

    ComPtr<ID3D11UnorderedAccessView> outputUAV;
    hResult = CreateBufferUAV(device, outputBuffer, outputUAV);


    if (FAILED(hResult))
    {
        return -1;
    }
    printf("Output UAV successfully created.\n");

#if defined(_DEBUG)
    if (inputView)
        inputView->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof("Buffer0 SRV") - 1, "Buffer0 SRV");
    if (outputUAV)
        outputUAV->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof("BufferOut UAV") - 1, "BufferOut UAV");
#endif


    //Set shader:
    deviceContext->CSSetShader(computeShader.Get(), nullptr, 0);
    deviceContext->CSSetShaderResources(0, 1, inputView.GetAddressOf());
    deviceContext->CSSetUnorderedAccessViews(0, 1, outputUAV.GetAddressOf(), nullptr);

    //Thread groups
    deviceContext->Dispatch(countVec.size(), 1, 1);

    //Unbind Input
    ComPtr<ID3D11ShaderResourceView> nullSRV = nullptr;
    deviceContext->CSSetShaderResources(0, 1, nullSRV.GetAddressOf());

    //Unbind Output
    ComPtr<ID3D11UnorderedAccessView> nullUAV = nullptr;
    deviceContext->CSSetUnorderedAccessViews(0, 1, nullUAV.GetAddressOf(), 0);

    //Disable Comput Shader
    deviceContext->CSSetShader(nullptr, nullptr, 0);

    ComPtr<ID3D11Buffer> resultBuffer = CreateAndRetrieveResultBuffer(device, deviceContext, outputBuffer);

    D3D11_MAPPED_SUBRESOURCE mappedResult;

    hResult = deviceContext->Map(resultBuffer.Get(), 0, D3D11_MAP_READ, 0, &mappedResult);

    int q = sizeof(BufType);

    vector<BufType> resultVec;

    if (SUCCEEDED(hResult))
    {

        //Works
        //BufType* p = (BufType*)mappedResult.pData;
        unique_ptr<BufType> resultDataPtr = unique_ptr<BufType>((BufType*)mappedResult.pData);
        for (int i = 0; i < countVec.size(); i++)
        {
            resultVec.push_back(resultDataPtr.get()[i]);
        }
        
        resultDataPtr.release();

        deviceContext->Unmap(resultBuffer.Get(), 0);
    }



    return 0;
}