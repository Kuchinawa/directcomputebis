struct StatWeights
{
    float weaponDMG;
    float intelligence;
    float accuracy;
    float criticalHitRate;
    float determination;
    float spellSpeed;
    float vitality;
    float piety;
};

struct MateriaCaps
{
    float accuracy;
    float criticalHitRate;
    float determination;
    float spellSpeed;
};

class GearItem
{
    float weaponDMG;
    float intelligence;
    float accuracy;
    float criticalHitRate;
    float determination;
    float spellSpeed;
    float vitality;
    float piety;
    int materiaSlots;

    MateriaCaps materiaCaps;
};

StructuredBuffer<GearItem> weapon : register(t0);
StructuredBuffer<GearItem> head : register(t1);
StructuredBuffer<GearItem> chest : register(t2);
StructuredBuffer<GearItem> hands : register(t3);
StructuredBuffer<GearItem> waist : register(t4);
StructuredBuffer<GearItem> legs : register(t5);
StructuredBuffer<GearItem> feet : register(t6);
StructuredBuffer<GearItem> neck : register(t7);
StructuredBuffer<GearItem> ears : register(t8);
StructuredBuffer<GearItem> wrists : register(t9);
StructuredBuffer<GearItem> rings : register(t10);


[numthreads(1, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{

}