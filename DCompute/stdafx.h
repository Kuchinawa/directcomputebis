// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

//Pre-processing directives
#define WIN32_LEAN_AND_MEAN

//Windows Include
#include <Windows.h>
#include <wrl.h>

#include <stdio.h>
#include <tchar.h>

//Include DirectX Libraries
#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <DirectXMath.h>

//Port D3DXMATH -> DirectXMath
//https://msdn.microsoft.com/en-us/library/windows/desktop/ff729728.aspx


//Include common libraries
#include <memory>
#include <string>
#include <vector>
#include <fstream>